#!/bin/bash

chmod 600 /tmp/id_rsa && cp /tmp/id_rsa /kubespray/id_rsa
cp /tmp/hosts.yaml /kubespray/inventory/sample/hosts.yaml
echo $VIP
sed -i "s/# loadbalancer_apiserver:/loadbalancer_apiserver:/g" /kubespray/inventory/sample/group_vars/all/all.yml
sed -i "s/#   address: 1.2.3.4/  address: $VIP/g" /kubespray/inventory/sample/group_vars/all/all.yml
sed -i "s/#   port: 1234/  port: 6443/g" /kubespray/inventory/sample/group_vars/all/all.yml

ansible-playbook -i /kubespray/inventory/sample/hosts.yaml --private-key id_rsa -e 'kubeconfig_localhost=true ansible_become=true ansible_user=ubuntu' /kubespray/scale.yml
